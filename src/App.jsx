import {Canvas} from '@react-three/fiber'
import {Experience} from './components/Experience'
import {Scroll, ScrollControls} from '@react-three/drei'
import {Interface} from './components/Interface.jsx'
import {Suspense, useEffect, useState} from 'react'
import {ScrollManager} from './components/ScrollManager.jsx'
import {Menu} from './components/Menu.jsx'
import {Leva} from 'leva'
import {MotionConfig} from 'framer-motion'
import {framerMotionConfig} from './config.js'

function App() {

  const [activePage, setActivePage] = useState(0)
  const [start, setStart] = useState(true)
  const [menuOpened, setMenuOpened] = useState(false)

  // useEffect(() => {
  //   if (start) {
  //     setStart(false)
  //     const timer = setTimeout(() => {
  //       setActivePage(0)
  //       clearTimeout(timer)
  //     }, 500)
  //   }
  // }, [start])

  useEffect(() => {
    setMenuOpened(false)
  }, [activePage])

  return (
    <>
      <MotionConfig
        transition={{
          ...framerMotionConfig
        }}
      >
        <Canvas shadows camera={{position: [4.27, 0.76, 1.56], fov: 42}}>
          <color attach="background" args={['#ececec']}/>
          <ScrollControls pages={4} damping={0.1}>
            <ScrollManager section={activePage} onSectionChange={setActivePage}/>
            <Scroll>
              <Suspense fallback={() => setActivePage(0)}>
                <Experience section={activePage}/>
              </Suspense>
            </Scroll>
            <Scroll html>
              <Interface onSectionChange={setActivePage}/>
            </Scroll>
          </ScrollControls>
        </Canvas>
        <Menu
          onSectionChange={setActivePage}
          menuOpened={menuOpened}
          setMenuOpened={setMenuOpened}
        />
      </MotionConfig>
      <Leva hidden/>
    </>


  )
}

export default App
