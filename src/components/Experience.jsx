import {
  ContactShadows,
  Environment,
  Float,
  Grid,
  MeshDistortMaterial,
  MeshWobbleMaterial,
  OrbitControls,
  Sky
} from '@react-three/drei'
import {motion} from 'framer-motion-3d'
import {Avatar} from './Avatar.jsx'
import {useControls} from 'leva'
import {OfficeChair} from './OfficeChair.jsx'
import * as THREE from 'three'
import {Room} from './Room.jsx'
import {useFrame, useThree} from '@react-three/fiber'
import {CuteIsometricRoom} from './CuteIsometricRoom.jsx'
// import {Witch} from './Witch.jsx'

export const Experience = ({section, ...props}) => {

  const {
    animation,
    chairPosX, chairPosY, chairPosZ, chairScale,
    roomPosX, roomPosY, roomPosZ, roomScale,
    ztPosX, ztPosY, ztPosZ, ztScale
  } = useControls({
    animation: {
      options: ['Typing', 'Standing', 'Falling'],
      value: 'Typing'
    },
    chairPosX: {value: 0, min: -10, max: 10, step: 0.01},
    chairPosY: {value: 0, min: -1, max: 1, step: 0.01},
    chairPosZ: {value: 0.12, min: -10, max: 10, step: 0.01},
    chairScale: {value: 1.42, min: 1, max: 2, step: 0.01},
    roomPosX: {value: -1.79, min: -2, max: 0, step: 0.01},
    roomPosY: {value: 0.82, min: 0, max: 1, step: 0.01},
    roomPosZ: {value: 0, min: -1, max: 0, step: 0.01},
    roomScale: {value: 1.7, min: 1.3, max: 1.7, step: 0.01},
    ztPosX: {value: -2.71, min: -3, max: -1, step: 0.01},
    ztPosY: {value: -0.08, min: -1.5, max: 0, step: 0.01},
    ztPosZ: {value: 6.54, min: 5, max: 8, step: 0.01},
    ztScale: {value: 3, min: 1, max: 5, step: 0.1}
  })

  const {viewport} = useThree()


  return (
    <>
      {/* <OrbitControls/> */}
      <motion.group
        animate={{
          z: section === 0 ? 6.5 : 0
        }}
      >
        <group position-z={-6.5} position-y={-1}>
          <ContactShadows
            opacity={0.42}
            scale={10}
            blur={1}
            far={10}
            resolution={256}
            color={'#000000'}
          />
          <group rotation-y={Math.PI} scale={chairScale} position={new THREE.Vector3(chairPosX, chairPosY, chairPosZ)}>
            {/* <Avatar animation={animation}/> */}
            {animation === 'Typing' && (<OfficeChair/>)}
          </group>
          >
          <Room section={section} scale={roomScale} position={new THREE.Vector3(roomPosX, roomPosY, roomPosZ)}/>
          <CuteIsometricRoom section={section} position-z={ztPosZ} position-x={ztPosX} position-y={ztPosY}
                             scale={ztScale} rotation-y={Math.PI * 0.5}/>
        </group>
      </motion.group>

      <motion.group
        position={[0, -1.5, -10]}
        animate={{
          x: section === 1 ? -20 : -20,
          z: section === 1 ? 10 : -20,
          y: section === 1 ? -viewport.height : -1.5
        }}
      >
        <directionalLight position={[-5, 3, 5]} intensity={0.4}/>
        <Float>
          <mesh position={[1, -3, -15]} scale={[2, 2, 2]}>
            <sphereGeometry/>
            <MeshDistortMaterial
              opacity={0.8}
              transparent
              distort={0.4}
              speed={4}
              color={'red'}
            />
          </mesh>
        </Float>
        <Float>
          <mesh scale={[3, 3, 3]} position={[3, 1, -18]}>
            <sphereGeometry/>
            <MeshDistortMaterial
              opacity={0.8}
              transparent
              distort={1}
              speed={5}
              color="yellow"
            />
          </mesh>
        </Float>
        <Float>
          <mesh scale={[1.4, 1.4, 1.4]} position={[-3, -1, -11]}>
            <boxGeometry/>
            <MeshWobbleMaterial
              opacity={0.8}
              transparent
              factor={1}
              speed={5}
              color={'blue'}
            />
          </mesh>
        </Float>
      </motion.group>
      <motion.group
        rotation-y={Math.PI}
        scale={chairScale}
        position={[0, 0, -1]}
        animate={{
          x : section === 0 ? 0 : 8,
          z : section === 0 ? 0.12 : -10,
          y: section === 0 ? -1 : 20
        }}
      >
        <Avatar animation={section === 0? 'Typing': 'Standing'} />
      </motion.group>

      {/* <ambientLight intensity={1}/> */}
      {/* <Grid infiniteGrid fadeDistance={50} fadeStrength={5}/> */}
      <Environment files={'/textures/venice_sunset_1k.hdr'}/>
      <Sky
        distance={450000}
        sunPosition={[5, 8, 20]}
        inclination={0}
        azimuth={0.25}
        rayleigh={0.1}
      />

    </>
  )
}
