import {motion} from 'framer-motion'

const Section = (props) => {
  const {children} = props
  return (
    <motion.section
      className={`h-screen w-screen p-8 max-w-screen-2xl mx-auto flex flex-col items-start justify-center`}
      initial={{
        opacity: 0,
        y: 50
      }}
      whileInView={{
        opacity: 1,
        y: 0,
        transition: {
          duration: 1,
          delay: 0.6
        }
      }}
    >
      {children}
    </motion.section>
  )
}

export const Interface = (props) => {
  const {onSectionChange} = props
  return (
    <>
      <div className="flex flex-col items-center w-screen">
        <AboutSection onSectionChange={onSectionChange}/>
        <SkillsSection />
        <ProjectsSection />
        <ContactSection />
      </div>
    </>
  )
}

const AboutSection = (props) => {
  const {onSectionChange} = props
  return (
    <Section>
      <>
        <h1 className="text-6xl font-extrabold leading-snug">
          嗨喽, 我是
          <br/>
          <span className="bg-pink-500 px-1 italic">{'天狼古城'}&nbsp;</span>
        </h1>
        <motion.p
          className="text-lg text-gray-600 mt-4 bg-white bg-opacity-45"
          initial={{
            opacity: 0,
            y: 25
          }}
          whileInView={{
            opacity: 1,
            y: 0
          }}
          transition={{
            duration: 1,
            delay: 1.5
          }}
        >
          我主要从事Java开发，同时也在学习JavaScript和TypeScript。
          <br/>
          我喜欢阅读书籍、看电影和打篮球。
          <br/>

          <br/>
          这是我学习THREE以及R3F后做的一个在线个人3D简历。
        </motion.p>
        <motion.button
          onClick={() => onSectionChange(3)}
          className={`bg-indigo-600 text-white py-4 px-8 
      rounded-lg font-bold text-lg mt-16`}
          initial={{
            opacity: 0,
            y: 25
          }}
          whileInView={{
            opacity: 1,
            y: 0
          }}
          transition={{
            duration: 1,
            delay: 2
          }}
        >
          联系我
        </motion.button>
      </>
    </Section>
  )
}

const skills = [
  {
    title: 'Spring / SpringBoot',
    level: 90
  },
  {
    title: 'Threejs / R3F',
    level: 80
  },
  {
    title: 'Vue / React',
    level: 90
  },
  {
    title: 'Nodejs',
    level: 70
  },
  {
    title: 'Typescript',
    level: 50
  },
  {
    title: '3D 模型',
    level: 40
  }
]
const languages = [
  {
    title: '♥️ zh 中文',
    level: 100
  },
  {
    title: '😂 🇺🇸 英文',
    level: 20
  }
]

const SkillsSection = () => {
  return (
    <Section>
      <motion.div whileInView={'visible'}>
        <h2 className="text-5xl font-bold text-white">技能</h2>
        <div className=" mt-8 space-y-4">
          {skills.map((skill, index) => (
            <div className="w-64" key={index}>
              <motion.h3
                className="text-xl font-bold text-gray-100"
                initial={{
                  opacity: 0
                }}
                variants={{
                  visible: {
                    opacity: 1,
                    transition: {
                      duration: 1,
                      delay: 1 + index * 0.2
                    }
                  }
                }}
              >
                {skill.title}
              </motion.h3>
              <div className="h-2 w-full bg-gray-200 rounded-full mt-2">
                <motion.div
                  className="h-full bg-indigo-500 rounded-full "
                  style={{width: `${skill.level}%`}}
                  initial={{
                    scaleX: 0,
                    originX: 0
                  }}
                  variants={{
                    visible: {
                      scaleX: 1,
                      transition: {
                        duration: 1,
                        delay: 1 + index * 0.2
                      }
                    }
                  }}
                />
              </div>
            </div>
          ))}
        </div>
        <div>
          <h2 className="text-5xl font-bold mt-10 text-white">语言</h2>
          <div className=" mt-8 space-y-4">
            {languages.map((lng, index) => (
              <div className="w-64" key={index}>
                <motion.h3
                  className="text-xl font-bold text-gray-100"
                  initial={{
                    opacity: 0
                  }}
                  variants={{
                    visible: {
                      opacity: 1,
                      transition: {
                        duration: 1,
                        delay: 2 + index * 0.2
                      }
                    }
                  }}
                >
                  {lng.title}
                </motion.h3>
                <div className="h-2 w-full bg-gray-200 rounded-full mt-2">
                  <motion.div
                    className="h-full bg-indigo-500 rounded-full "
                    style={{width: `${lng.level}%`}}
                    initial={{
                      scaleX: 0,
                      originX: 0
                    }}
                    variants={{
                      visible: {
                        scaleX: 1,
                        transition: {
                          duration: 1,
                          delay: 2 + index * 0.2
                        }
                      }
                    }}
                  />
                </div>
              </div>
            ))}
          </div>
        </div>
      </motion.div>
    </Section>
  )
}

const ProjectsSection = () => {
  return (
    <Section>
      <div className="flex w-full h-full gap-8 items-center justify-center">
        <button
          className="hover:text-indigo-600 transition-colors"
          // onClick={previousProject}
        >
          ← 上一个
        </button>
        <h2 className="text-5xl font-bold">项目</h2>
        <button
          className="hover:text-indigo-600 transition-colors"
          // onClick={nextProject}
        >
          下一个 →
        </button>
      </div>
    </Section>
  )
}

const ContactSection = () => {
  return (
    <Section>
      <>
        <h2 className="text-5xl font-bold">联系我</h2>
        <div className="mt-8 p-8 rounded-md bg-white w-96 max-w-full">
          <form>
            <label htmlFor="name" className="font-medium text-gray-900 block mb-1">
              姓名
            </label>
            <input
              type="text"
              name="name"
              id="name"
              className="block w-full rounded-md border-0 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 p-3"
            />
            <label
              htmlFor="email"
              className="font-medium text-gray-900 block mb-1 mt-8"
            >
              邮箱
            </label>
            <input
              type="email"
              name="email"
              id="email"
              className="block w-full rounded-md border-0 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 p-3"
            />
            <label
              htmlFor="email"
              className="font-medium text-gray-900 block mb-1 mt-8"
            >
              消息
            </label>
            <textarea
              name="message"
              id="message"
              className="h-32 block w-full rounded-md border-0 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 p-3"
            />
            <button className="bg-indigo-600 text-white py-4 px-8 rounded-lg font-bold text-lg mt-16 ">
              提交
            </button>
          </form>
        </div>
      </>
    </Section>
  )
}
