# 使用官方的 Nginx 镜像作为基础镜像
FROM nginx:1.19.10

# 将本地的 index.html 文件复制到 Nginx 的默认站点目录下
COPY dist/ /usr/share/nginx/html/

# 暴露 80 端口
EXPOSE 80

# 启动 Nginx
ENTRYPOINT ["nginx", "-g", "daemon off;"]

# docker build -t my-nginx .
# docker run -d -p 8449:80 my-nginx
