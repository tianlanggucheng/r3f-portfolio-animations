# r3f-vite-starter
A boilerplate to build R3F projects

```
yarn
yarn dev
```


![image](https://user-images.githubusercontent.com/6551176/221732091-23ee52cb-4150-42fa-b998-43628d7a6b0d.png)

## 控制台报错 
### Uncaught TypeError: Cannot read properties of undefined (reading 'length')

```shell
# 控制台报错 Uncaught TypeError: Cannot read properties of undefined (reading 'length')
Uncaught TypeError: Cannot read properties of undefined (reading 'length')
    at Object.update (chunk-ARVSLCLW.js?v=48cc622c:11525:46)
    at setProgram (chunk-ARVSLCLW.js?v=48cc622c:18802:22)
    at WebGLRenderer.renderBufferDirect (chunk-ARVSLCLW.js?v=48cc622c:18147:23)
    at renderObject (chunk-ARVSLCLW.js?v=48cc622c:18568:15)
    at renderObjects (chunk-ARVSLCLW.js?v=48cc622c:18550:11)
    at renderScene (chunk-ARVSLCLW.js?v=48cc622c:18474:9)
    at WebGLRenderer.render (chunk-ARVSLCLW.js?v=48cc622c:18376:9)
    at render$1 (chunk-RJDL3HZZ.js?v=48cc622c:17557:14)
    at loop (chunk-RJDL3HZZ.js?v=48cc622c:17575:19)
```

### 解决 

```shell
# 卸载当前版本
npm uninstall -g gltfjsx
npm i -g gltfjsx@6.2.11
# 还是会报错，全局缺少依赖
# Cannot find package 'read-pkg-up' imported from D:\develop\nodejs\node_global\node_modules\gltfjsx\cli.js
npm i read-pkg-up

npx gltfjsx@6.2.11 .\public/models\646d9dcdc8a5f5bddbfac913.glb -o .\src/components/Avatar01.jsx -r .\public
```
